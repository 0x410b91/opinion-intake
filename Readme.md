# Opinion Intake

## Local setup and run
Supported only on WSL, MacOS and Linux

### Installation
Simply run ```pip install -r requirements.txt```

### Running it 
Run the following command: ```gunicorn -b 127.0.0.1:80 src.app:api``` 
and access http://localhost/ using your favorite REST client

### Example
```
POST http://localhost/
{
	"urls": ["https://www.debate.org/opinions/should-drug-users-be-put-in-prison", "https://www.debate.org/opinions/is-youtube-a-platform-for-individuals", "https://www.debate.org/opinions/in-terms-of-grahics-moddability-and-price-do-you-think-minecraft-is-a-good-game"]
}	
```

## Docker container
Supported on all platforms with docker

### Building 
```docker build -t opinion-intake:latest .```

### Running
```docker docker run -it -p 8080:8080 opinion-intake:latest```

### Example 
```
POST http://localhost:8080/
{
	"urls": ["https://www.debate.org/opinions/should-drug-users-be-put-in-prison", "https://www.debate.org/opinions/is-youtube-a-platform-for-individuals", "https://www.debate.org/opinions/in-terms-of-grahics-moddability-and-price-do-you-think-minecraft-is-a-good-game"]
}	
```
