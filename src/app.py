import json
import urllib
import falcon
import requests
from src.data import Opinion
from src.schemas import OpinionInputSchema
from falcon.media.validators import jsonschema
from urllib3.exceptions import NewConnectionError

class OpinionResource:
  @jsonschema.validate(OpinionInputSchema)
  def on_post(self, req, resp):
    try:
      opinions = [Opinion(url).__dict__ for url in req.media.get("urls")]
      resp.media = {"status": "ok", "opinions": opinions}
    except requests.exceptions.ConnectionError as e:
      resp.status = falcon.HTTP_500
      resp.media = {"status": "error", "description": "One or more of the URLs supplied is incorrect or points to a server that isn't responding"}
    except urllib.error.HTTPError as e:
      resp.status = falcon.HTTP_500
      resp.media = {"status": "error", "description": "One or more of the URLs supplied is unaccessible (404)"}
    except AttributeError as e: 
      resp.status = falcon.HTTP_500
      resp.media = {"status": "error", "description": "One or more of the URLs supplied do not point to a debate"}
      

api = falcon.API()
api.add_route('/', OpinionResource())