import re
import requests
from pyquery import PyQuery as pq

re_percent_extract = re.compile("^(.+)(%)")

class Opinion(object):
  def __init__(self, url):
    try:
      document = pq(url=url)
    except requests.exceptions.MissingSchema as e:
      document = pq(url=f'http://{url}')
    self.name = document(".q-title").text()
    self.category = document("#breadcrumb a").eq(-2).text() # Second to last breadcrumb represents a general category of opinion
    self.percentage_yes = int(re_percent_extract.match(document(".yes-text").text()).group(1))
    self.percentage_no = int(re_percent_extract.match(document(".no-text").text()).group(1))
    # For arguments, the last list element is actually used as a display element and doesn't contain data, we trim it out
    self.yes_arguments = [{"title": argument("h2").text(), "text": argument("p").text()} for argument in document.items("#yes-arguments li")][:-1]
    self.no_arguments = [{"title": argument("h2").text(), "text": argument("p").text()} for argument in document.items("#no-arguments li")][:-1]
    self.related_opinions = [f"https://www.debate.org{link.attrib['href']}" for link in document(".related-opinion-page .list-container ul li a")]