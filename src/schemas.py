OpinionInputSchema = {
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/product.schema.json",
  "title": "Opinion Input",
  "description": "The input to the opinion extraction api endpoint (a list of URLs)",
  "type": "object",
  "properties": {
    "urls": {
      "description": "URLs to the opinions",
      "type": "array",
      "items": {
        "type": "string"
      },
      "minItems": 1,
      "uniqueItems": False
    }
  }
}
