import os
import sys
import json
import pytest
from falcon import testing, HTTP_500

sys.path.append(os.path.realpath(os.path.dirname(__file__)+"/..")) # Needed to be able to just call pytest from the root of the project
from src.app import api

@pytest.fixture()
def client():
  return testing.TestClient(api)

def test_known_debate_argument_has_title_and_text(client):
  doc = {
    "urls": ["https://www.debate.org/opinions/should-drug-users-be-put-in-prison"]
  }

  result = client.simulate_post('/', body=json.dumps(doc))
  assert result.json.get('opinions')[0].get("yes_arguments")[0].get("title") != ""
  assert result.json.get('opinions')[0].get("yes_arguments")[0].get("text") != ""

def test_known_debate_has_arguments(client):
  doc = {
    "urls": ["https://www.debate.org/opinions/should-drug-users-be-put-in-prison"]
  }

  result = client.simulate_post('/', body=json.dumps(doc))
  assert len(result.json.get('opinions')[0].get("yes_arguments")) > 0

def test_name_extracted_from_page_is_correct(client):
  doc = {
    "urls": ["https://www.debate.org/opinions/should-drug-users-be-put-in-prison"]
  }

  result = client.simulate_post('/', body=json.dumps(doc))
  assert result.json.get('opinions')[0].get("name") == "Should drug users be put in prison?"

def test_category_extracted_from_page_is_correct(client):
  doc = {
    "urls": ["https://www.debate.org/opinions/should-drug-users-be-put-in-prison"]
  }

  result = client.simulate_post('/', body=json.dumps(doc))
  assert result.json.get('opinions')[0].get("category") == "Society"

def test_multiple_urls_passed_returns_same_number_responses(client):
  doc = {
    "urls": ["https://www.debate.org/opinions/should-drug-users-be-put-in-prison", 
              "https://www.debate.org/opinions/is-youtube-a-platform-for-individuals", 
              "https://www.debate.org/opinions/in-terms-of-grahics-moddability-and-price-do-you-think-minecraft-is-a-good-game"]
  }

  result = client.simulate_post('/', body=json.dumps(doc))
  assert len(result.json.get('opinions')) == len(doc.get('urls'))

def test_url_typo_triggers_error_with_description(client):
  doc = {
    "urls": ["https://www.debate.org/opinions/in-terms-of-grahics-moddability-and-price-do-you-think-minecraft-is-a-good-gam"]
  }

  result = client.simulate_post('/', body=json.dumps(doc))
  assert result.status == HTTP_500
  assert result.json.get('status') == "error"
  assert result.json.get('description') != ""

def test_url_not_pointing_to_debate_triggers_error_with_description(client):
  doc = {
    "urls": ["https://www.debate.org/opinions/"]
  }

  result = client.simulate_post('/', body=json.dumps(doc))
  assert result.status == HTTP_500
  assert result.json.get('status') == "error"
  assert result.json.get('description') != ""